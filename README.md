**That's just a draft!**

## What

opening sockets then dropping privileges is a very common pattern in
secure programming.
This project is a helper about that.

`dropper.py` is the "parent". It must be run as root. It will open the
specified sockets and pass them to the child.

`child.py` is a process receiving open FDs. There is some code to
enumerate them and create a python socket object from the raw FD

## How to run

```sh
sudo ./dropper.py -c TCP:www.riseup.net:80 -c UNIX:my.sock $USER ./child.py
```
