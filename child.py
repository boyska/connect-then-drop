#!/usr/bin/python3
import os
import socket
import stat
import time


def main():
    print("\n".join(k for k in os.environ.keys()))
    print("=>", os.getenv("INHERIT_FD"))
    fd_dir = "/proc/%d/fd/" % os.getpid()

    sockets = []
    for fname in os.getenv('INHERIT_FD').split(','):
        fd = int(fname)
        fpath = os.path.join(fd_dir, fname)
        if not os.path.exists(fpath):
            continue
        fstat = os.fstat(fd)
        if stat.S_ISREG(fstat.st_mode):
            print("trying file", fname)
            buf = os.fdopen(fd, "w")
            buf.write('%.2f\n' % time.time())
            buf.close()
        elif stat.S_ISSOCK(fstat.st_mode):
            print("trying socket", fname)
            s = socket.socket(fileno=fd)
            print(s)
            sockets.append(s)
        else:
            print("Skipping", fname)

    print(len(sockets), 'sockets')
    for s in sockets:
        print("\n\n", s)
        s.send(b"GET /\r\n")
        r = s.recv(200)
        s.close()
        print(r.decode("ascii"))
        print("\n")


if __name__ == "__main__":
    main()
